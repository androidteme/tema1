package com.example.firstapp

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*
import kotlinx.android.synthetic.main.fragment_one.*

class SecondActivity : AppCompatActivity() {

    var isFragmentOneLoaded = true
    val manager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        /*val Change = findViewById<Button>(R.id.btn_change)
        ShowFragmentOne()
        Change.setOnClickListener({
            if(isFragmentOneLoaded)
                ShowFragmentTwo()
            else
                ShowFragmentOne()
        })*/

        btn_1.setOnClickListener{
            Screen.setBackgroundColor(Color.RED) }

        btn_2.setOnClickListener{
            Screen.setBackgroundColor(Color.YELLOW)
        }

        btn_3.setOnClickListener {
            Screen.setBackgroundColor(Color.BLUE)
        }
    }

    fun ShowFragmentOne(){
        val transaction = manager.beginTransaction()
        val fragment = Fragment1()
        transaction.replace(R.id.fragment_holder,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
        isFragmentOneLoaded = true
    }
    fun ShowFragmentTwo(){
        val transaction = manager.beginTransaction()
        val fragment = Fragment2()
        transaction.replace(R.id.fragment_holder,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
        isFragmentOneLoaded = false
    }
}
